import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../screens/map_screen.js.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter GoogleMap',
      theme: ThemeData(
        primaryColor: Colors.blue
      ),
      home: MapScreen(),
    );
  }
}
